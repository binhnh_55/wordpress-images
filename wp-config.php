<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bitbitbit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234567');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?Avq_t#6|BFfagj@4?f4mq7jHW=iIV4ejE<a~muF]bB4k?N~HG-gV>Y>p7kCu%8W');
define('SECURE_AUTH_KEY',  '!7*:]|DgPs?M[m-.NP41H>_YEu+%i0WQrw,E*Va0R[^6I2-X7m!9L}bUBPE@FiIf');
define('LOGGED_IN_KEY',    '^hSf}DbvJYx8LCxB,mv/2t=p7e|Da1Guhm4&OnM|wk. -XI!}z&O`$=#ir@1+zLr');
define('NONCE_KEY',        '%yQb9+DDUV[mCoV&qW^XMsw2D9wso4q4^Db-j[wmKDv{:sG[j?YCGL#{)|Yy{lYt');
define('AUTH_SALT',        'Bj5TVZVK.(|6CO:XI7SWs~csO03Om PCRtgVnPhQ;w7`V2*9k^ommoENwa!/dX_2');
define('SECURE_AUTH_SALT', '*50mUs?_CaS_pu6|R>-5cDtAiy]^FndmH]ijh(?O<-2Q%cGGyeZV2f8[=()wE!BV');
define('LOGGED_IN_SALT',   '=jZ}~+- $Bq<E<-4UA=yuV6O,CU`R1$33UwQ{<-7%HxZ4%rf7:uN*t<)AN>A./X5');
define('NONCE_SALT',       'h/0GXr7Ljj:H!jZ%u!>ExiTi6;7Tn(ae<BJ!.76ho^^qQ2nUw.>IF}KRBD>4p116');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
