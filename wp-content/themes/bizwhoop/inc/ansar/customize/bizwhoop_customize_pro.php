<?php
//Pro Button

function bizwhoop_pro_customizer( $wp_customize ) {
    class bizwhoop_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    
    public function render_content() {
    ?>
     <div class="ta-pro-box">
       <a href="<?php echo esc_url( __('https://themeansar.com/themes/bizwhoop/','bizwhoop'));?>" target="_blank" class="upgrade" id="review_pro"> <?php _e( 'Upgrade To Pro','bizwhoop' ); ?></a>
        
    </div>
    <?php
    }
}
    $wp_customize->add_section( 'bizwhoop_pro_section' , array(
		'title' => __('Upgrade to pro', 'bizwhoop'),
		'priority' => 1000,
   	) );

    $wp_customize->add_setting('upgrade_pro', array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw',
    ) );

    $wp_customize->add_control( new bizwhoop_Customize_Control( $wp_customize, 'upgrade_pro', array(
	    'section' => 'bizwhoop_pro_section',
		'setting' => 'upgrade_pro',)
    ) );

class bizwhoop_Review_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
	<div class="ta-pro-box">
        <a href="<?php echo esc_url( __('https://wordpress.org/support/view/theme-reviews/bizwhoop#postform/', 'bizwhoop'));?>" target="_blank" class="review" id="review_pro"><?php _e( 'Support Forum','bizwhoop' ); ?></a>
	</div>
    <?php
    }
}

    $wp_customize->add_setting( 'pro_Review', array(
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw',
    ) );
    $wp_customize->add_control( new bizwhoop_Review_Customize_Control( $wp_customize, 'pro_Review', array(	
	    'section' => 'bizwhoop_pro_section',
		'setting' => 'pro_Review',)
    ) );



    class bizwhoop_tastore_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
      <div class="ta-pro-box">
     <a href="<?php echo esc_url( __('https://themeansar.com/themes/', 'bizwhoop'));?>" target="_blank" class="tastore" id="ta_store"><?php _e( 'Explore Our Themes','bizwhoop' ); ?></a>
     
     <div>
    <?php
    }
}

    
	$wp_customize->add_setting( 'ti_store', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'esc_url_raw',
    ) );
    $wp_customize->add_control( new bizwhoop_tastore_Customize_Control( $wp_customize, 'ti_store', array(  
        'label' => __('Discover bizwhoop Pro','bizwhoop'),
        'section' => 'bizwhoop_pro_section',
        'setting' => 'ti_store',)
    ) );

}
add_action( 'customize_register', 'bizwhoop_pro_customizer' );
?>